# Ordenar un stack usando otro stack y una sola variable temporal.

stack1 = [37, 48, 18, 53, 19, 64, 42, 95, 1]
stack2 = [1,1,1,1,1,1,1,2,1]

def ordenar(stackA):
    ''' Para ordenar el stack A, se moveran sus elementos a un stack temporal
    B de tal manera que B siempre esté ordenado.

    Si se quiere mover un elemento que desordenaria B, el elemento se guardara
    en una variable temporal t y se devolveran los elementos de B a A hasta
    que se pueda ingresar el elemento guardado en t.'''

    stackB = []

    while len(stackA) > 0:
        t = stackA.pop()
        while len(stackB) > 0 and stackB[-1] > t:
            stackA.append(stackB.pop())

        stackB.append(t)

    while len(stackB) > 0:
        stackA.append(stackB.pop())
    return stackA

assert ordenar(stack1) == [95, 64, 53, 48, 42, 37, 19, 18, 1]
assert ordenar(stack2) == [2, 1, 1, 1, 1, 1, 1, 1, 1]
    

# Suma de numeros al interior de Strings
def sumNumbers(s):
    ''' Para resolver esta pregunta se avanzara por cada uno de los chars
    del string agregando los numeros a un string y separandolos cada vez
    que haya un caracter que no sea numerico entre medio.

    Como el enunciado indica que cada numero se interpreta de la forma mas
    larga posible, se considera que el ejemplo sumNumbers('711') = 18 es
    incorrecto, pues la forma mas larga de interpretar el numero es 711.'''
    
    numbers = []
    currentNum = ''
    for letra in s:
        if letra.isalpha():
            if currentNum == '':
                continue
            else:
                numbers.append(currentNum)
                currentNum = ''
        else:
            currentNum += letra
    if currentNum is not '':
        numbers.append(currentNum)

    resultado = 0
    for n in numbers:
        resultado += int(n)

    return resultado
                

assert sumNumbers('abc123xyz') == 123
assert sumNumbers('aa11b33') == 44
assert sumNumbers('711') == 711


# Caracter K-dominante
def kDominante(s, c):
    ''' Para buscar el menor k para el cual C es k-dominante en S podemos
    buscar la mayor distancia entre dos repeticiones de C.

    Como para buscar la mayor distancia recorremos cada elemento del
    string solo una vez y en orden, el algoritmo tambien funciona para
    streams.'''

    maxDistancia = 0
    distanciaActual = 0
    for letra in s:
        if letra is c:
            maxDistancia = max(maxDistancia, distanciaActual)
            distanciaActual = 0
        else:
            distanciaActual += 1

    return maxDistancia + 1

assert kDominante('abfdshabdsabdhsa', 'a') == 6
assert kDominante('abbabbbababbbaba', 'a') == 4
    

