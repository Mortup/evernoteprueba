# Prueba Tecnica
## Parte 1 - Problemas de Computación
### Ordenar un stack usando otro stack y una sola variable temporal
Para ordenar el stack A, se moveran sus elementos a un stack temporal B de tal manera que B siempre esté ordenado.

Si se quiere mover un elemento que desordenaria B, el elemento se guardara en una variable temporal t y se devolveran los elementos de B a A hasta que se pueda ingresar el elemento guardado en t.

### Suma de numeros al interior de Strings
Para resolver esta pregunta se avanzara por cada uno de los chars del string agregando los numeros a un string y separandolos cada vez que haya un caracter que no sea numerico entre medio.

Como el enunciado indica que cada numero se interpreta de la forma mas larga posible, se considera que el ejemplo sumNumbers('711') = 18 es incorrecto, pues la forma mas larga de interpretar el numero es 711.

### Caracter K-dominante
Para buscar el menor k para el cual C es k-dominante en S podemos buscar la mayor distancia entre dos repeticiones de C.

Como para buscar la mayor distancia recorremos cada elemento del string solo una vez y en orden, el algoritmo tambien funciona para streams. La única diferencia es que cada caracter que revisemos hay que sacarlo
del stream uno a la vez.

## Parte 2 - Scraping
Para realizar el scraping se decidió buscar libros dentro de la lista de libros [mas populares](https://scraping-test.herokuapp.com/book/add/), pues se supone que estos son mas relevantes. Del top 100 se recogieron los 50 mejores.

El procedimiento seguido fue: imprimir el contenido de un campo para todos los libros y revisar su formato, luego hacer cambios al contenido obtenido para limpiar los caracteres extra y ajustarse al formato solicitado. Esto se repitió para cada campo. Se consideró crear una función para abstraer este procedimiento, pero como cada campo tenia un formato distinto se prefirió hacerlo manualmente.

Cuando habia un libro al cual le faltaban campos, estos se reemplazaron por el texto _Not available_ o el entero _-1_ dependiendo del formato solicitado. Se escogieron estos campos para que quede claro que el campo no fue encontrado.

Luego, se procedió a enviar las request para ingresar los datos. Para esto se tuvo que crear una sesion para mantener la cookie de login, agregar un header de _referer_, pues el sitio lo pedia y hacer un request get antes de enviar los datos para obtener el token csrf.

Al revisar las respuestas de las request post todo parecia haberse ingresado correctamente.