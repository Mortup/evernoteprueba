import requests
from bs4 import BeautifulSoup

NOT_FOUND = 'Not available'

topBooksReq = requests.get(url='https://www.gutenberg.org/browse/scores/top')
topBooksSoup = BeautifulSoup(topBooksReq.text, 'lxml')

links = [x['href'] for x in topBooksSoup.find('h2', {'id': 'books-last1'}).find_next('ol').find_all('a')]

user = 'guribe'
pswd = 'PwDj7cVqFABj3aHhfXBAkpUW465P45Ve'
s = requests.Session()
s.headers.update({'referer': 'https://scraping-test.herokuapp.com/book/add/'})
loginCsrf = s.get('https://scraping-test.herokuapp.com/book/add/')
logCsrfToken = BeautifulSoup(loginCsrf.text, 'lxml').find('input', {'name': 'csrfmiddlewaretoken'}).get('value')
loginReq = s.post('https://scraping-test.herokuapp.com/users/login/', data={'csrfmiddlewaretoken': logCsrfToken, 'username': user, 'password': pswd})

counter = 1
for link in links:
  bookReq = requests.get(url='https://www.gutenberg.org' + link)
  bookSoup = BeautifulSoup(bookReq.text, 'lxml')

  table = bookSoup.find('table', {'class': 'bibrec'})
  title = table.find('th', text='Title').find_next('td').get_text('\n').split('\n')[1]
  bookId = link.split('/')[2]
  try:
    authorInfo = table.find('th', text='Author').find_next('td').get_text('\n').split('\n')[2]
    authorName = authorInfo.rpartition(',')[0]
    authorBirth = authorInfo.rpartition(',')[2].split('-')[0].lstrip()
    authorDeath = authorInfo.rpartition(',')[2].split('-')[1].rstrip()
  except:
    authorName = NOT_FOUND
    authorBirth = -1
    authorDeath = -1

  try:
    editor = table.find('th', text='Editor').find_next('td').get_text('\n').split('\n')[-1].rpartition(',')[0]
  except:
    editor = NOT_FOUND

  try:
    illustrator = table.find('th', text='Illustrator').find_next('td').get_text('\n').split('\n')[-1].rpartition(',')[0]
  except:
    illustrator = NOT_FOUND

  try:
    note = table.find('th', text='Note').find_next('td').text
  except:
    note = NOT_FOUND

  try:
    contents = table.find('th', text='Contents').find_next('td').get_text('\n')
  except:
    contents = NOT_FOUND

  try:
    alternateTitle = table.find('th', text='Alternate Title').find_next('td').get_text('\n').split('\n')[-2]
  except:
    alternateTitle = NOT_FOUND

  try:
    language = table.find('th', text='Language').find_next('td').get_text('\n').split('\n')[-1]
  except:
    language = NOT_FOUND

  try:
    loc = table.find(lambda tag:tag.name=='th' and 'Class' in tag.text).find_next('td').get_text('\n').split('\n')[2]
  except:
    loc = NOT_FOUND

  try:
    subject = table.find('th', text='Subject').find_next('td').get_text('\n').split('\n')[3]
  except:
    subject = NOT_FOUND

  try:
    category = table.find('th', text='Category').find_next('td').get_text('\n')
  except:
    category = NOT_FOUND

  try:
    ebookno = table.find('th', text='EBook-No.').find_next('td').get_text('\n')
  except:
    ebookno = -1

  try:
    releaseDate = table.find(lambda tag:tag.name=='th' and 'Release' in tag.text).find_next('td').get_text('\n')
  except:
    releaseDate = NOT_FOUND

  try:
    copyrightStatus = table.find(lambda tag:tag.name=='th' and 'Copyright' in tag.text).find_next('td').get_text('\n')
  except:
    copyrightStatus = NOT_FOUND

  try:
    downloads = table.find('th', text='Downloads').find_next('td').get_text('\n').split(' ')[0]
  except:
    downloads = -1

  try: 
    price = table.find('th', text='Price').find_next('td').get_text('\n')[1:].split('.')[0]
  except:
    price = -1

  try:
    qrinfo = bookSoup.find(lambda tag:tag.name=='style' and 'qr' in tag.text).get_text('\n').split('(')[1].split(')')[0]
    qrinfo = 'https://www.gutenberg.org' + qrinfo
  except:
    qrinfo = NOT_FOUND

  #print(f'{counter}) {title}')
  #print(qrinfo)
  csrfReq = s.get('https://scraping-test.herokuapp.com/book/add/')
  csrftoken = BeautifulSoup(csrfReq.text, 'lxml').find('input', {'name': 'csrfmiddlewaretoken'}).get('value')
  data = {
     'book_id': bookId,
     'author_name': authorName,
     'author_birth_year': authorBirth,
     'author_death_year': authorDeath,
     'editor': editor,
     'illustrator': illustrator,
     'title': title,
     'note': note,
     'contents': contents,
     'alternate_title': alternateTitle,
     'language': language,
     'loc_class': loc,
     'subject': subject,
     'category': category,
     'ebook_no': ebookno,
     'release_date': releaseDate,
     'copyright_status': copyrightStatus,
     'downloads': downloads,
     'price': price,
     'qr_info': qrinfo,
     'test': True,
     'csrfmiddlewaretoken': csrftoken,
  }
  s.headers.update({'referer': 'https://scraping-test.herokuapp.com/book/add/'})
  addReq = s.post(url='https://scraping-test.herokuapp.com/book/add/', data=data)
  
  print(f'Submited info for {title}')
  if counter == 50:
    break

  counter += 1
